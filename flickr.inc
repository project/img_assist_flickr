<?php


function _flickr_query($params) {
  $encoded_params = array();
  foreach ($params as $k => $v) {
    $encoded_params[] = urlencode($k) . '=' . urlencode($v);
  }
  $url = "http://api.flickr.com/services/rest/?" . implode('&', $encoded_params);
  $rsp = file_get_contents($url);
  return unserialize($rsp);
}

function _flickr_search($page, $terms) {
  if ($page == '') {
    $page = 1;
  }
  else { 
    $page = (int)$page + 1;
  }
  $params = array(
    'api_key'  => variable_get('img_assist_flickr_key', ''),
    'method'   => 'flickr.photos.search',
    'format'   => 'php_serial',
    'per_page' => variable_get('img_assist_preview_count', 10),
    'page'     => $page,
    'tags'     => $terms,
    'license'  => '1,2,3,4,5,6',
    'group_id' => variable_get('img_assist_flickr_group', ''),
  );
  return _flickr_query($params);
}

function _flickr_get_licenses() {
  $params = array(
    'api_key'	  => variable_get('img_assist_flickr_key', ''),
    'method'	  => 'flickr.photos.licenses.getInfo',
    'format'	  => 'php_serial'
  );
  $result = _flickr_query($params);
  $result = $result['licenses']['license'];
  $licenses = array();
  foreach ($result as $license) {
    $licenses[$license['id']] = $license['name']; 
  }
  
  return $licenses;
}

function _flickr_get_sizes($photo_id) {
  $params = array(
    'api_key'	  => variable_get('img_assist_flickr_key', ''),
    'method'	  => 'flickr.photos.getSizes',
    'photo_id'	=> $photo_id,
    'format'	  => 'php_serial'
  );
  return _flickr_query($params);
}

function _flickr_get_info($photo_id, $secret) {
  $params = array(
    'api_key'  => variable_get('img_assist_flickr_key', ''),
    'method'   => 'flickr.photos.getInfo',
    'photo_id' => $photo_id,
    'format'   => 'php_serial',
    'secret'   => $secret
  );
  return _flickr_query($params);
}
