
function initFlickrProperties() {
  parent.setHeader('properties');
  parent.updateCaption();
  onChangeFlickrSizeLabel();
}

function onChangeFlickrSizeLabel() {
  var formObj = parent.frames['img_assist_main'].document.forms[0];
  var size = formObj['edit-size-label'].value.split('x');
  var width = size[0];
  var height = size[1];
  formObj['edit-width'].value = width;
  formObj['edit-height'].value = height;
  label = formObj['edit-size-label'].options[formObj['edit-size-label'].selectedIndex].text;
  label = label.substring(0,label.indexOf('(')-1);
  formObj['edit-size'].value = label;
}
